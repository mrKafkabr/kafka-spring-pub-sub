package springpubsub.springpubsub.producer;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class Producer {

    private final String BOOTSTRAP_SERVERS;

    public Producer(@Value("${kafka.bootstrapAddress}") String bootstrap_server) {
        this.BOOTSTRAP_SERVERS = bootstrap_server;
    }

    public void send(String topic, String payload) {

        Properties properties = new Properties();
        //kafka bootstrap server
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty("key.serializer", StringSerializer.class.getName());
        properties.setProperty("value.serializer", StringSerializer.class.getName());
        //producer acks
        properties.setProperty(ProducerConfig.ACKS_CONFIG, "1");
        properties.setProperty("retries", "3");

        org.apache.kafka.clients.producer.Producer<String, String> producer = new org.apache.kafka.clients.producer.KafkaProducer<>(properties);

        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topic, payload);
        producer.send(producerRecord);
        producer.flush();

        producer.close();
    }
}
