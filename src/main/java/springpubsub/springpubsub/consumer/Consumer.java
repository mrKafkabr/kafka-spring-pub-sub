package springpubsub.springpubsub.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import springpubsub.springpubsub.producer.Producer;

@Component
public class Consumer {

    private final String TOPIC = "DEFINIR O TOPICO AQUI";

    private final String bootstrap;

    @Autowired
    public Consumer(@Value("${kafka.bootstrapAddress}") String bootstrap_servers) {
        this.bootstrap = bootstrap_servers;
    }

    @KafkaListener(topics = {TOPIC})
    public void listenerInsert(@Payload String message) {
     //para enviar para o kafka, utilizar o metodo abaixo
    //sendoToKafka();
    }

    private void sendToKafka(String payload, String topic) {
        Producer producer = new Producer(bootstrap);
        producer.send(topic,payload);
    }


}
